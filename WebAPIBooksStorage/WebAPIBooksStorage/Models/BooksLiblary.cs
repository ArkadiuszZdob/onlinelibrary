﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPIBooksStorage.Models
{
    public class BooksLiblary
    {
        private static BooksLiblary repo = new BooksLiblary();

        public static BooksLiblary Current{
            get
            { return repo; }
        }

        private List<Book> DataBook = new List<Book>
        { new Book { Id=1,Title="Lord of The Rings",Author="J.R.R. Tolkien",PublishHouse="Nova"},
        new Book { Id=2,Title="Silmarilion",Author="J.R.R. Tolkien",PublishHouse="Nova" },
        new Book { Id=3,Title="Hobbit",Author="J.R.R. Tolkien",PublishHouse="Nova" },
        new Book { Id=4,Title="Witcher",Author="A.Sapkowski",PublishHouse="Sova" }
        };

        public IEnumerable<Book> GetAll()
        {
            return DataBook;
        }

        public Book Get(int id)
        {
            return DataBook.Where(i => i.Id == id).FirstOrDefault();
        }

        public Book Add(Book item)
        {
            item.Id = DataBook.Count + 1;
            DataBook.Add(item);
            return item;
        }

        public bool Update(Book item)
        {
            Book currentBook = Get(item.Id);
            if(currentBook!=null)
            {
                currentBook.Title = item.Title;
                currentBook.Author = item.Author;
                currentBook.PublishHouse = item.PublishHouse;
                return true;
            }
            else { return false; }
        }

        public void Remove(int item)
        {
            Book itemToRemove = Get(item);
            DataBook.Remove(itemToRemove);
        }
    }
}