﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using WebAPIBooksStorage.Models;

namespace WebAPIBooksStorage.Controllers
{
    public class WebController : ApiController
    {
        private ReservationRepository repo = ReservationRepository.Current;

        public IEnumerable<Reservation> GetAllReservations()
        {
            return repo.GetAll();
        }
        public Reservation GetReservation(int i)
        {
            return repo.Get(i);
        }
        [HttpPost]
        public Reservation CreateReservarion(Reservation item)
        {
            return repo.Add(item);
        }

        [HttpPut]
        public bool UpdateReservation(Reservation item)
        {
            return repo.Update(item);
        }

        public void DeleteReservation(int item)
        {
            repo.Remove(item);
        }
        
    }
}