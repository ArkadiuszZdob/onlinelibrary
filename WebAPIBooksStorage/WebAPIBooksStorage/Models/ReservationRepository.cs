﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPIBooksStorage.Models
{
    public class ReservationRepository
    {
        private static ReservationRepository repo = new ReservationRepository();

        public static ReservationRepository Current
        {
            get
            { return repo; }
        }

        private List<Reservation> DataReservation = new List<Reservation>
        { new Reservation{ ReservationId=1,ClientName="Lord of The Rings",Location="J.R.R. Tolkien"},
        new Reservation { ReservationId=2,ClientName="Silmarilion",Location="J.R.R. Tolkien" },
        new Reservation { ReservationId=3,ClientName="Hobbit",Location="J.R.R. Tolkien"},
        new Reservation { ReservationId=4,ClientName="Witcher",Location="A.Sapkowski"}
        };

        public IEnumerable<Reservation> GetAll()
        {
            return DataReservation;
        }

        public Reservation Get(int id)
        {
            return DataReservation.Where(i => i.ReservationId == id).FirstOrDefault();
        }

        public Reservation Add(Reservation item)
        {
            item.ReservationId = DataReservation.Count + 1;
            DataReservation.Add(item);
            return item;
        }

        public bool Update(Reservation item)
        {
            Reservation currentBook = Get(item.ReservationId);
            if (currentBook != null)
            {
                currentBook.Location = item.Location;
                currentBook.ClientName = item.ClientName;
                return true;
            }
            else { return false; }
        }

        public void Remove(int item)
        {
            Reservation itemToRemove = Get(item);
            DataReservation.Remove(itemToRemove);
        }
    }
}