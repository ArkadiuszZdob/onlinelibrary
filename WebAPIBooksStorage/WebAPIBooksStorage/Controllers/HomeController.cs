﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Web.Http;
using WebAPIBooksStorage.Models;

namespace WebAPIBooksStorage.Controllers
{
    public class HomeController : Controller
    {
        private BooksLiblary repo = BooksLiblary.Current;
        /*
        public IEnumerable<Book> GetAllBooks()
        {
            return repo.GetAll();
        }
        [HttpPost]
        public Book AddBook(Book item)
        {
            return repo.Add(item);
        }
        
        public void RemoveBook(Book item)
        {
            repo.Remove(item);
        }
        [HttpPut]
        public bool UpdateBook(Book item)
        {
            return repo.Update(item);
        }
        public Book GetBook(int i)
        {
            return repo.Get(i);
        }
        */

        public ViewResult Index()
        {
            return View(repo.GetAll());
        }

        public ActionResult Add(Book item)
        {
            if(ModelState.IsValid)
            {
                repo.Add(item);
                return RedirectToAction("Index");
            }
            else
            {
                return View("Index");
            }
        }

        public ActionResult Remove(int id)
        {
            repo.Remove(id);
            return RedirectToAction("Index");
        }

        public ActionResult Update(Book item)
        {
            if(ModelState.IsValid && repo.Update(item))
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View("Index");
            }
        }
    }
}